import {SHOW_TOPIC} from "../actions/";

export default function (state = {}, action) {
    switch (action.type) {
        case SHOW_TOPIC:
            return action.payload;
        default:
            return state;
    }
}