import { combineReducers } from 'redux';
import TopicsReducer from './reducer_topics';
import TopicReducer from './reducer_topic';

const rootReducer = combineReducers({
    topics: TopicsReducer,
    topic: TopicReducer,
});

export default rootReducer;