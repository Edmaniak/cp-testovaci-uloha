import _ from 'lodash';
import {FETCH_TOPICS} from "../actions";
import {ADD_TOPIC} from "../actions";

export default function (state = {}, action) {
    switch (action.type) {
        case FETCH_TOPICS:
            const sorted = _.orderBy(action.payload.data[0].topics, ['creationDate'],['desc']);
            return sorted;
        case ADD_TOPIC:
            return [action.payload, ...state];
        default:
            return state;
    }
}