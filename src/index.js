import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {BrowserRouter, Route} from 'react-router-dom';
import promise from 'redux-promise';
import reducers from './reducers';

import TopicDetail from './components/TopicDetail/TopicDetail';
import AddPage from './components/AddPage/AddPage';
import TopicsTable from "./components/TopicsTable/TopicsTable";
import AppHeader from './components/AppHeader/AppHeader';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <div id="container">
                <Route path="/" component={AppHeader}/>
                <Route exact path="/" component={TopicsTable}/>
                <Route path="/add" component={AddPage}/>
                <Route path="/topic/:id" component={TopicDetail}/>
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root')
);