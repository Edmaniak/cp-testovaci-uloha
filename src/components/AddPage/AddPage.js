import React, {Component} from 'react';
import './addPage.scss';
import {connect} from 'react-redux';
import {addTopic} from "../../actions/index";

class AddPage extends Component {

    handleSubmit(event) {
        event.preventDefault();

        const today = new Date();
        const values = event.target;
        const id = Math.floor(Math.random() * (10000));

        const newTopic = {
            temporary: true,
            title: values.title.value,
            createdBy: {
                lastname: values.author.value
            },
            creationDate: today.getTime(),
            keywords: values.keywords.value,
            id: id

        };

        // run the action for topic state change and redirect to main
        this.props.addTopic(newTopic);
        this.props.history.push("/");
    }

    render() {
        return (
            <form onSubmit={(event) => this.handleSubmit(event)} className="addForm">
                <input name="title" placeholder="Title"/>
                <input name="author" placeholder="Author"/>
                <input name="keywords" placeholder="Keywords"/>
                <button>ADD NEW</button>
            </form>
        )
    }
}

export default connect(null, {addTopic})(AddPage);