import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchTopics} from '../../actions';
import {showTopic} from "../../actions/index";
import _ from "lodash";
import './topicsTable.scss';


class TopicsTable extends Component {

    componentDidMount() {
        if (_.isEmpty(this.props.topics))
            this.props.fetchTopics();
    }

    handleClick(e,topic) {
        this.props.showTopic(topic);
        this.props.history.push(`/topic/${topic.id}`);
    }

    renderTopics() {
        return _.map(this.props.topics, topic => {

            const date = new Date(topic.creationDate);
            const dateF = `${date.getDay()}. ${date.getMonth()}. ${date.getFullYear()}`;

            return ( <tr onClick={(e) => this.handleClick(e,topic)} key={topic.id} className={topic.temporary ? "new-topic" : ""}>
                <td>
                    {topic.id}
                </td>
                <td>
                    {topic.title}
                </td>
                <td>
                    {dateF}
                </td>
                <td>
                    {topic.createdBy.lastname}
                </td>
                <td>
                    {topic.keywords}
                </td>
            </tr>)
        })
    }

    render() {
        // Fetching the topics -> show loading wheel
        if (_.isEmpty(this.props.topics))
            return <div className="loader"><img src={require("../../res/loader.svg")}/></div>;
        // Fetching done -> show table
        else
            return (
                <div className="table-container end">
                    <table>
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Author</th>
                            <th>Keywords</th>
                        </tr>
                        </thead>
                        <tbody>{this.renderTopics()}</tbody>
                    </table>
                </div>
            )
    }
}

function mapStateToProps(state) {
    return {topics: state.topics};
}

export default connect(mapStateToProps, {fetchTopics,showTopic})(TopicsTable);