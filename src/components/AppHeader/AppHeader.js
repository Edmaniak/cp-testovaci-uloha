import React, {Component} from 'react';
import './header.scss';
import {Link} from 'react-router-dom';
import '../../res/new.svg';

class AppHeader extends Component {

    // Main heading changes dynamically slo by lepe.
    renderHead() {
        switch(this.props.location.pathname) {
            case "/add":
                return "ADD";
            default:
                return "TOPICS";
        }
    }

    renderNav() {
       if(this.props.location.pathname !== "/")
           return <Link className="back-arrow" to="/">&#129092;</Link>
    }

    render() {
        return (
            <div className="app-header">
                {this.renderNav()}
                <Link to="/add"><img className="new-button" src={require("../../res/new.svg")}/></Link>
                <h1>{this.renderHead()}</h1>
            </div>
        )
    }
}

export default AppHeader;