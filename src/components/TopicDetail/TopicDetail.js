import React, {Component} from 'react';
import {connect} from 'react-redux';
import './topicsDetail.scss';
import _ from 'lodash';


class TopicDetail extends Component {

    renderDetails(prop, key) {
        if (!_.isObject(prop)) {
            return (
                <tr key={key}>
                    <td className="key"> {`${key}:`}</td>
                    <td className="value">{String(prop)}</td>
                </tr>
            )
        }
    }

    render() {
        const {topic} = this.props;
        return (
            <div>
                <div className="detail">
                    <h2>{`${topic.title}`}</h2>
                </div>
                <div className="detail end">
                    <table className="detail-table" >
                        <tbody>
                        <tr>
                            <td className="key">author:</td>
                            <td className="value">{topic.createdBy.lastname}</td>
                        </tr>
                        {_.map(topic, (prop, key) => this.renderDetails(prop, key))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        topic: state.topic
    }
}

export default connect(mapStateToProps)(TopicDetail);