import axios from 'axios';

// Action constants names
export const FETCH_TOPICS = 'fetch_topics';
export const ADD_TOPIC = 'add_topic';
export const SHOW_TOPIC = 'show_topic';

// Fetch topics from remote api
export function fetchTopics() {
    const request = axios.get("http://private-ad5ec-cp19.apiary-mock.com/topics");
    return {
        type: FETCH_TOPICS,
        payload: request
    }
}

// Add topic
export function addTopic(values) {
    return {
        type: ADD_TOPIC,
        payload: values
    }
}

// Show individual topic (topic is a state of app)
export function showTopic(topic) {
    return {
        type: SHOW_TOPIC,
        payload: topic
    }
}
